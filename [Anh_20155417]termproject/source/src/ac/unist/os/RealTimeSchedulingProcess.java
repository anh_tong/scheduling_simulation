package ac.unist.os;

/**
 * Created by anhth on 6/12/17.
 */
public class RealTimeSchedulingProcess implements Comparable<RealTimeSchedulingProcess>{

    private int processId;
    private int period;
    private int processingTime;

    public RealTimeSchedulingProcess(int processId, int period, int processingTime) {
        this.processId = processId;
        this.period = period;
        this.processingTime = processingTime;
    }

    public int getProcessId() {
        return processId;
    }

    public int getPeriod() {
        return period;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    @Override
    public int compareTo(RealTimeSchedulingProcess that) {
        return Integer.compare(this.getPeriod(), that.getPeriod());
    }
}
