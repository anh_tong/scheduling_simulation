package ac.unist.os;

/**
 * Created by anhth on 6/11/17.
 */
public class RoundRobinScheduler extends AbstractScheduler {

    private int quantum;
    private int consumedTime;

    public RoundRobinScheduler(Queue inputQueue, int quantum) {
        super(inputQueue);
        this.quantum = quantum;
    }

    @Override
    public void schedule() {

    }

    @Override
    public Process nextOneMillisecond(int inputTime) {
        this.updateReadyQueue(inputTime);
        if (!this.running){
            if (inputTime != 0 && this.currentProcess.getRemaining() != 0){
//                System.out.println(
//                        Utils.printTerminate(inputTime, this.currentProcess.getProcessId())
//                );
                this.readyProcesses.addProcess(this.currentProcess);
            }

            if (this.readyProcesses.isEmpty()){
                return null;
            }
            this.consumedTime = 0;
            this.running = true;
            this.setCurrentProcess();
        }
        return workAt(inputTime);
    }

    @Override
    public Process workAt(int inputTime) {
        this.currentProcess.workAt(inputTime);
//        if (this.consumedTime == this.quantum && this.currentProcess.getStartTime() != inputTime){
//            System.out.println(
//                    Utils.printSchedule(inputTime + 1, this.currentProcess.getProcessId())
//            );
//        }
        this.consumedTime++;
        if (this.consumedTime == this.quantum ||
                this.currentProcess.getRemaining() == 0){
            running = false;
        }

//        else if (this.currentProcess.getStartTime() == inputTime){
//            System.out.println(
//                    Utils.printSchedule(inputTime, this.currentProcess.getProcessId())
//            );
//        }
        return this.currentProcess;
    }
}
