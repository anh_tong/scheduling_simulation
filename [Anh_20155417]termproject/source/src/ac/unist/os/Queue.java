package ac.unist.os;

import java.util.ArrayList;

/**
 * Created by anhth on 6/11/17.
 */
public class Queue {

    private ArrayList<Process> processes;

    public Queue() {
        this.processes = new ArrayList<>();
    }

    public void orderByArrive(){
        //TODO: order by arrival time
    }

    public void orderBySJF(){
        // TODO: order by SJF
    }

    public boolean isEmpty(){
        return processes.isEmpty();
    }

    public int size(){
        return this.processes.size();
    }

    public Process getProcess(int i){
        return this.processes.get(i);
    }

    public void addProcess(Process p){
        this.processes.add(p);
    }

    public void removeProcess(int i){
        this.processes.remove(i);
    }

}
