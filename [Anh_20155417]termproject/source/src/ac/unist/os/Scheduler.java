package ac.unist.os;

/**
 * Created by anhth on 6/12/17.
 */
public interface Scheduler {

    public Process nextOneMillisecond(int inputTime);

    public void schedule();
}
