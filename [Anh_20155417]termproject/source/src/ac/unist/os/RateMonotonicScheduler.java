package ac.unist.os;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by anhth on 6/12/17.
 */
public class RateMonotonicScheduler implements Scheduler {

    private List<RealTimeSchedulingProcess> allProcesses;

    private int endTime;

    public RateMonotonicScheduler(List<RealTimeSchedulingProcess> allProcesses, int endTime) {
        this.allProcesses = allProcesses;
        this.endTime = endTime;
    }

    @Override
    public Process nextOneMillisecond(int inputTime) {
        return null;
    }

    public void schedule(){
        ArrayList<RealTimeSchedulingProcess> waitList = new ArrayList<>();

        int previousId = -1;
        for (int cpuTime = 0; cpuTime < this.endTime; cpuTime++) {

            for(RealTimeSchedulingProcess p: this.allProcesses){
                if (cpuTime % p.getPeriod() == 0){
                    waitList.add(p);
                }
            }
            if (!waitList.isEmpty()){
                Collections.sort(waitList);

                int selectedId = waitList.get(0).getProcessId();
                if (selectedId != previousId){
                    if (previousId == -1) {
                        System.out.println((cpuTime + 1) + " : scheduled P" + selectedId);
                    } else {
                        System.out.println((cpuTime + 1) + " : terminated P" + previousId);
                        System.out.println((cpuTime + 1) + " : scheduled P" + selectedId);
                    }
                    previousId = selectedId;
                }
                waitList.remove(0);
            }
        }
    }
}
