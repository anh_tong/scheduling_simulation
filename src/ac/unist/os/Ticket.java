package ac.unist.os;

/**
 * Created by anhth on 6/12/17.
 */
public class Ticket {
    private int number;
    private LotteryProcess process;

    public Ticket(int number, LotteryProcess process) {
        this.number = number;
        this.process = process;
    }

    public int getNumber() {
        return number;
    }

    public LotteryProcess getProcess() {
        return process;
    }
}
