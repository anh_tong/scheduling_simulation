package ac.unist.os;

import java.util.ArrayList;

/**
 * Created by anhth on 6/11/17.
 */
public class Queue {

    private ArrayList<Process> processes;

    public Queue() {
        this.processes = new ArrayList<>();
    }

    public void orderByArrive(){
        for (int i = 0; i < processes.size(); i++) {
            for (int j = i + 1; j < processes.size(); j++) {
                Process p1 = processes.get(i);
                Process p2 = processes.get(j);

                if (p2.getArrivalTime() < p1.getArrivalTime()){
                    processes.set(i, p2);
                    processes.set(j, p1);
                }

            }
        }
    }

    public void orderBySJF(){
        for (int i = 0; i < processes.size(); i++) {
            for (int j = i + 1; j < processes.size(); j++) {
                Process p1 = processes.get(i);
                Process p2 = processes.get(j);

                if (p2.getBurstTime() < p1.getBurstTime()){
                    processes.set(i, p2);
                    processes.set(j, p1);
                }

            }
        }
    }

    public boolean isEmpty(){
        return processes.isEmpty();
    }

    public int size(){
        return this.processes.size();
    }

    public Process getProcess(int i){
        return this.processes.get(i);
    }

    public void addProcess(Process p){
        this.processes.add(p);
    }

    public void removeProcess(int i){
        this.processes.remove(i);
    }

}
