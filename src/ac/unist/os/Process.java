package ac.unist.os;

/**
 * Created by anhth on 6/11/17.
 */
public class Process {

    private int processId;
    private int arrivalTime;
    private int burstTime;

    private int startTime; //actual start time

    private boolean finished;
    private int finish;
    private int remaining;

    public Process(int processId, int arrivalTime, int burstTime) {
        this.processId = processId;
        this.arrivalTime = arrivalTime;
        this.burstTime = burstTime;

        this.finish = 0;
        this.remaining = burstTime;
    }

    public int getProcessId() {
        return processId;
    }

    public int getFinish() {
        if (finished)
            return finish;
        return 0;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getRemaining(){
        return this.remaining;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getBurstTime() {
        return burstTime;
    }

    public void workAt(int inputTime){
        if (this.burstTime == this.remaining){
            this.startTime = inputTime;
            System.out.println(String.valueOf(inputTime) + ": scheduled P" + String.valueOf(this.processId));
        }
        this.remaining --;
        if (this.remaining == 0){
            this.finish = inputTime + 1;
            this.finished = true;
            System.out.println(String.valueOf(inputTime + 1) + ": terminated P" + String.valueOf(this.processId));
        }
    }
}
