package ac.unist.os;


import java.util.*;

/**
 * Created by anhth on 6/11/17.
 */
public class LotteryScheduler extends AbstractScheduler {

    private int quantum;
    private int consumedTime;
    private boolean force;
    private List<Ticket> tickets;
    private Random rand;

    private Map<Integer, Process> holdingResource;

    public LotteryScheduler(Queue inputQueue, int quantum, boolean force) {
        super(inputQueue);
        this.quantum = quantum;
        this.force = force;
        this.tickets = new ArrayList<>();
        this.rand = new Random();
        this.holdingResource = new HashMap<>();
    }

    @Override
    public void schedule() {

    }

    @Override
    public Process nextOneMillisecond(int inputTime) {
        // based on arrival time we update this queue
        this.updateReadyQueue(inputTime);
        if (!this.running){
            if (inputTime != 0 && this.currentProcess.getRemaining() != 0){
                // put back to the queue
                this.readyProcesses.addProcess(this.currentProcess);
            }
            if (this.readyProcesses.isEmpty()){
                return null;
            }
            this.consumedTime = 0;
            this.running = true;
            // do lotter here to set the current process
            lottery();

        }
        return workAt(inputTime);
    }

    @Override
    public Process workAt(int inputTime) {
        this.currentProcess.workAt(inputTime);
        this.consumedTime++;
        if (this.consumedTime == this.quantum ||
                this.currentProcess.getRemaining() == 0){
            running = false;
            //release resource
            if (this.currentProcess.getRemaining() == 0) {
                LotteryProcess temp = (LotteryProcess) this.currentProcess;
                this.holdingResource.remove(temp.getResouceType());
            }
        }

        if (this.consumedTime == this.quantum){
            System.out.println(
                    Utils.printSchedule(inputTime + 1, this.currentProcess.getProcessId())
            );
        }


        return this.currentProcess;
    }

    @Override
    protected void updateReadyQueue(int inputTime) {
        super.updateReadyQueue(inputTime);

        // get resource typ
    }

    public void lottery(){
        //
        if (force){
            // select the process having the highest ticket
            LotteryProcess luckyProcess = new LotteryProcess(-1, -1, -1, Integer.MIN_VALUE);
            for (int i = 0; i < this.readyProcesses.size(); i++) {
                LotteryProcess p = (LotteryProcess) readyProcesses.getProcess(i);
                if (p.getNumTickets() > luckyProcess.getNumTickets()){
                    luckyProcess = p;
                }
            }

            //if any process holding resource
            if (luckyProcess.getResouceType() > 0){
                if (holdingResource.containsKey(luckyProcess.getResouceType())){
                    luckyProcess = (LotteryProcess) holdingResource.get(luckyProcess.getResouceType());
                }else {
                    holdingResource.put(luckyProcess.getResouceType(), luckyProcess);
                }
            }

            // here the highest one is the lucky one.
            int order = -1;
            for (int i = 0; i < this.readyProcesses.size(); i++) {
                LotteryProcess p = (LotteryProcess) readyProcesses.getProcess(i);

                if (p.getProcessId() == luckyProcess.getProcessId()) {
                    order = i;
                    break;
                }
            }


            this.readyProcesses.removeProcess(order);
            this.currentProcess = luckyProcess;

        }else {
            // allocate tickets
            tickets.clear();
            for (int i = 0; i < this.readyProcesses.size(); i++) {
                LotteryProcess p = (LotteryProcess)readyProcesses.getProcess(i);

                for (int j = 0; j < p.getNumTickets() ; j++) {
                    this.tickets.add(new Ticket(tickets.size(), p));
                }
            }
            // random ticket
            int winningTicket = rand.nextInt(tickets.size());
            Process luckyProcess = tickets.get(winningTicket).getProcess();
            int order = -1;
            for (int i = 0; i < this.readyProcesses.size(); i++) {
                LotteryProcess p = (LotteryProcess) readyProcesses.getProcess(i);

                if (p.getProcessId() == luckyProcess.getProcessId()){
                    order = i;
                    break;
                }
            }

            this.readyProcesses.removeProcess(order);
            this.currentProcess = luckyProcess;
        }
    }
}



