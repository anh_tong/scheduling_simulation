package ac.unist.os;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by anhth on 6/11/17.
 */
public class Simulator {


    public static void main(String[] args) {

        // argument parser
        Options  options = new Options();

        Option inputOpt = new Option("i", "input", true, "input file");
        inputOpt.setRequired(true);
        options.addOption(inputOpt);

        Option schedulerOpt = new Option("s", "scheduler", true, "scheduler algorithm");
        schedulerOpt.setRequired(true);
        options.addOption(schedulerOpt);

        Option quantumOpt = new Option("q", "quantum", true, "quantum time for RR and/or LT");
        quantumOpt.setRequired(false);
        options.addOption(quantumOpt);

        Option endTimeOpt = new Option("e", "end", true, "end time for RM and EDF");
        endTimeOpt.setRequired(false);
        options.addOption(endTimeOpt);

        Option forceOpt = new Option("f", "force", false, "end time for RM and EDF");
        forceOpt.setRequired(false);
        options.addOption(forceOpt);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try{
            cmd = parser.parse(options, args);
        }catch (ParseException e){

            formatter.printHelp("utility-name", options);
            e.printStackTrace();
            return;
        }

        String inputFile = cmd.getOptionValue("input");
        String schedulerStr = cmd.getOptionValue("scheduler");

        Stream<String> inputLines;
        try {
            inputLines = Files.lines(FileSystems.getDefault().getPath(inputFile));
        } catch (IOException e) {
            System.out.println("Cannot locate file: " + inputFile);
            e.printStackTrace();
            return;
        }

        switch (schedulerStr){
            case "SJF":
                // Shortest Job First
                Stream<Process> processes = inputLines.map(Utils::getProcessFromStr);
                Queue inputQueue = new Queue();
                processes.forEach(inputQueue::addProcess);
                AbstractScheduler schedulerSJF = new ShortestJobFirstScheduler(inputQueue);
                int timeSJF = 0;
                while (!schedulerSJF.isFinished()) {
                    schedulerSJF.nextOneMillisecond(timeSJF);
                    timeSJF++;
                }

                break;
            case "RR":
                // Round Robin
                String quantumTime;
                int qt;
                try {
                    quantumTime = cmd.getOptionValue("quantum");
                }catch  (Exception e){
                    System.out.println("Please input quatum time [-q] for RR case");
                    e.printStackTrace();
                    return;
                }
                try {
                    qt = Integer.valueOf(quantumTime);
                }catch (Exception e){
                    System.out.println("Please input quatum time [-q] for RR case");
                    e.printStackTrace();
                    return;
                }

                Stream<Process> processesRR = inputLines.map(Utils::getProcessFromStr);
                Queue inputQueueRR = new Queue();
                processesRR.forEach(inputQueueRR::addProcess);
                //TODO: Missing some outputs
                AbstractScheduler schedulerRR = new RoundRobinScheduler(inputQueueRR, qt);

                int time = 0;
                while (!schedulerRR.isFinished()) {
                    schedulerRR.nextOneMillisecond(time);
                    time++;
                }
                break;
            case "RM":
                // Rate Monotonic
                int endTime;
                try{
                    endTime = Integer.valueOf(cmd.getOptionValue("end"));
                }catch (Exception e){
                    System.out.println("Please input end time [-e] for RM and EDF case");
                    return;
                }

                List<RealTimeSchedulingProcess> processRM = inputLines.map(Utils::getRTProcessFromStr).collect(Collectors.toList());
                RateMonotonicScheduler schedulerRM = new RateMonotonicScheduler(processRM, endTime);
                schedulerRM.schedule();
                break;
            case "EDF":
                // Earliest Deadline First
                int endTimeEDF;
                try{
                    endTimeEDF = Integer.valueOf(cmd.getOptionValue("end"));
                }catch (Exception e){
                    System.out.println("Please input end time [-e] for RM and EDF case");
                    return;
                }

                List<RealTimeSchedulingProcess> processEDF = inputLines.map(Utils::getRTProcessFromStr).collect(Collectors.toList());
                EarliestDeadlineFirstScheduler schedulerEDF = new EarliestDeadlineFirstScheduler(processEDF, endTimeEDF);
                schedulerEDF.schedule();
                break;
            case "LT":
                // Lottery
                int qtLT;
                try {
                    qtLT = Integer.valueOf(cmd.getOptionValue("quantum"));
                }catch (Exception e){
                    System.out.println("Please input quatum time [-q] for LT case");
                    e.printStackTrace();
                    return;
                }
                boolean force = cmd.hasOption("f");

                Stream<Process> processesLT = inputLines.map(Utils::getLotteryProcessFromStr);
                Queue inputQueueLT = new Queue();
                processesLT.forEach(inputQueueLT::addProcess);
                AbstractScheduler schedulerLT = new LotteryScheduler(inputQueueLT, qtLT, force);
                int t = 0;
                while (!schedulerLT.isFinished()) {
                    schedulerLT.nextOneMillisecond(t);
                    t++;
                }
                break;
        }

    }


}
