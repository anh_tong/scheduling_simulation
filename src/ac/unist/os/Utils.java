package ac.unist.os;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by anhth on 6/12/17.
 */
public class Utils {

    public static String printSchedule(int time, int processId){
        return time + ": scheduled P" + processId;
    }

    public static String printTerminate(int time, int processId){
        return time + ": terminated P" + processId;
    }

    public static Process getProcessFromStr(String line){
        String[] splitted = line.split(" ");
        try {
            return new Process(
                    Integer.valueOf(splitted[0]),
                    Integer.valueOf(splitted[1]),
                    Integer.valueOf(splitted[2])
                    );
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    public static RealTimeSchedulingProcess getRTProcessFromStr(String line){
        String[] splitted = line.split(" ");
        try {
            return new RealTimeSchedulingProcess(
                Integer.valueOf(splitted[0]),
                    Integer.valueOf(splitted[1]),
                    Integer.valueOf(splitted[2])
            );
        }catch (Exception e){
            e.printStackTrace();
            throw  e;
        }
    }

    public static LotteryProcess getLotteryProcessFromStr(String line){
        String[] splitted = line.split(" ");
        try {
            LotteryProcess lp = new LotteryProcess(
                    Integer.valueOf(splitted[0]),
                    Integer.valueOf(splitted[1]),
                    Integer.valueOf(splitted[2]),
                    Integer.valueOf(splitted[3])
            );
            if (splitted.length == 5){
                lp.setResouceType(Integer.valueOf(splitted[4]));
            }
            return lp;
        }catch (Exception e){
            e.printStackTrace();
            throw  e;
        }
    }
}
