package ac.unist.os;

/**
 * Created by anhth on 6/11/17.
 */
public class ShortestJobFirstScheduler extends AbstractScheduler {

    public ShortestJobFirstScheduler(Queue inputQueue) {
        super(inputQueue);
    }

    @Override
    public Process nextOneMillisecond(int inputTime) {
        this.updateReadyQueue(inputTime);
        if (this.readyProcesses.size() > 1) {
            this.readyProcesses.orderBySJF();
        }

        if (!this.running){
            if(readyProcesses.isEmpty())
                return null;
            this.running = true;
            this.setCurrentProcess();
        }
        return workAt(inputTime);
    }

    @Override
    public Process workAt(int inputTime) {
        this.currentProcess.workAt(inputTime);
        if(currentProcess.getRemaining() == 0){
            this.running = false;
        }
        return this.currentProcess;
    }

    @Override
    public void schedule() {

    }
}
