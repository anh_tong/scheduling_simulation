package ac.unist.os;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by anhth on 6/12/17.
 */
public class EarliestDeadlineFirstScheduler implements Scheduler{

    private List<RealTimeSchedulingProcess> allProcesses;

    private int endTime;

    public EarliestDeadlineFirstScheduler(List<RealTimeSchedulingProcess> allProcesses, int endTime) {
        this.allProcesses = allProcesses;
        this.endTime = endTime;
    }

    @Override
    public Process nextOneMillisecond(int inputTime) {
        return null;
    }

    @Override
    public void schedule() {
        HashMap<Integer, ArrayList<RealTimeSchedulingProcess>> waitingMap = new HashMap<>();
        int previousId = -1;
        for (int cpuTime = -1; cpuTime < this.endTime; cpuTime++) {
            // make deadlines of all process
            for (RealTimeSchedulingProcess p : allProcesses) {
                if (cpuTime + 1 < endTime && (cpuTime + 1) % p.getPeriod() == 0) {
                    if (!waitingMap.containsKey(cpuTime + 1 + p.getPeriod())) {
                        waitingMap.put(cpuTime + 1 + p.getPeriod(), new ArrayList<>());
                    }
                    for (int i = 0; i < p.getProcessingTime(); i++) {
                        waitingMap.get(cpuTime + 1 + p.getPeriod()).add(p);
                    }
                }
            }

            // pick the earliest deadline
            if (!waitingMap.isEmpty()) {
                Integer minKey = waitingMap.keySet().stream().min(Integer::compareTo).get();
                int selectedId = waitingMap.get(minKey).get(0).getProcessId();
                //if the processes are switched, write out the information
                if (selectedId != previousId) {
                    if (previousId == -1) {
                        System.out.println((cpuTime + 1) + " : scheduled P" + selectedId);
                    } else {
                        System.out.println((cpuTime + 1) + " : terminated P" + previousId);
                        System.out.println((cpuTime + 1) + " : scheduled P" + selectedId);
                    }
                    previousId = selectedId;
                }
                waitingMap.get(minKey).remove(0);
                if (waitingMap.get(minKey).isEmpty()) {
                    waitingMap.remove(minKey);
                }
            }
        }
    }


}
