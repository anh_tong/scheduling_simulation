package ac.unist.os;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anhth on 6/12/17.
 */
public class RateMonotonicScheduler_old{

    private List<RealTimeSchedulingProcess> allProcesses;

    private int endTime;

    public RateMonotonicScheduler_old(List<RealTimeSchedulingProcess> allProcesses, int endTime) {
        this.allProcesses = allProcesses;
        this.endTime = endTime;
    }


    public void schedule(){
        ArrayList<Integer> cycle = new ArrayList<>();
        for (int i = 0; i < 2*endTime; i++) {
            cycle.add(0);
        }

        int lastExecution = 0;
        for (int m = 0; m < allProcesses.size(); m++) {
            int place = 0;
            RealTimeSchedulingProcess currentProcess = allProcesses.get(m);
            int processingTime = currentProcess.getProcessingTime();
            int period = currentProcess.getPeriod();

            int rep = (int) (endTime) / period;
            if (lastExecution != 0){
                for (int before = 0; before < lastExecution; before++) {
                    place ++;
                }
            }

            for (int r = 0; r < rep; r ++){
                boolean print = true;
                int executionTime = 0;
                do {
                    if (cycle.get(place) != null && cycle.get(place).equals(1)){
                        place ++;
                    } else {
                        if (print) {
                            System.out.println(String.valueOf(place) + ": scheduled P" + String.valueOf(currentProcess.getProcessId()));
                            print = false;
                        }
                        cycle.set(place, 1);
                        place ++;
                        executionTime ++;
                    }
                }while (executionTime < processingTime);
                System.out.println(String.valueOf(place) + ": terminated P" + String.valueOf(currentProcess.getProcessId()));
                print = false;

                for (int s = 0; s <= (processingTime - executionTime); s++) {
                    place ++;
                }
            }
            lastExecution = lastExecution + processingTime;
        }
        
    }



    private int lcm(){
        int lcm = allProcesses.get(0).getPeriod();

        for (boolean flag = true; flag; ){
            for (RealTimeSchedulingProcess p: allProcesses){
                if (lcm % p.getPeriod() != 0){
                    flag = true;
                    break;
                }
            }
            lcm = flag? (lcm + 1):lcm;
        }
        return lcm;
    }
}
