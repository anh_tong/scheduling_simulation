package ac.unist.os;

/**
 * Created by anhth on 6/12/17.
 */
public class LotteryProcess extends Process {

    private int numTickets;
    private int resouceType = -1;

    public LotteryProcess(int processId, int arrivalTime, int burstTime, int numTickets) {
        super(processId, arrivalTime, burstTime);
        this.numTickets = numTickets;
    }

    public int getResouceType() {
        return resouceType;
    }

    public void setResouceType(int resouceType) {
        this.resouceType = resouceType;
    }

    public int getNumTickets() {
        return numTickets;
    }
}
