package ac.unist.os;

/**
 * Created by anhth on 6/11/17.
 */
public abstract class AbstractScheduler implements Scheduler{

    protected Queue allProcesses;
    protected Queue readyProcesses;
    protected Process currentProcess;

    protected boolean running;

    public AbstractScheduler(Queue inputQueue){
        this.readyProcesses = new Queue();
        this.running = false;
        this.allProcesses = inputQueue;
        this.allProcesses.orderByArrive();
        this.currentProcess = new Process(-1,-1,-1);
    }

    public abstract Process nextOneMillisecond(int inputTime);

    public abstract void schedule();

    public Process workAt(int inputTime){
        this.currentProcess.workAt(inputTime);
        return this.currentProcess;
    }

    public boolean isFinished(){
        return (allProcesses.isEmpty() &&
                readyProcesses.isEmpty() &&
                !running &&
                currentProcess.getRemaining() == 0);
    }

    protected void updateReadyQueue(int inputTime){
        for (int i = 0; i < allProcesses.size(); i++) {
            Process p = allProcesses.getProcess(i);

            if(p.getArrivalTime() == inputTime){
                readyProcesses.addProcess(p);
                allProcesses.removeProcess(i);
                i --;
            }
        }

    }

    protected void setCurrentProcess(){
        this.currentProcess = readyProcesses.getProcess(0);
        this.readyProcesses.removeProcess(0);
    }
}
